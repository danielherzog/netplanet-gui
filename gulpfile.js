// including plugins
var gulp = require('gulp');
var uglify = require('gulp-uglify');
var concat = require('gulp-concat');
var sourcemaps = require('gulp-sourcemaps');
var nano = require('gulp-cssnano');
var less = require('gulp-less');
var gutil = require('gulp-util');
var rimraf = require('gulp-rimraf');
var jshint = require('gulp-jshint');

var directories = {
    app: 'public/js/**/*.js'
};

//tasks
gulp.task('default', ['build-less', 'build-css', 'copy-fonts', 'build-js', 'build-angular', 'jshint', 'build-app']);

gulp.task('clean', function () {
    return gulp.src('public/dist/*')
        .pipe(rimraf({force: true}));
});

gulp.task('build-less', function () {
    return gulp.src('public/less/**/*.less')
        .pipe(less())
        .pipe(gulp.dest('public/css'))
});

gulp.task('copy-fonts', function () {
    return gulp.src([
            'public/libs/bootstrap/fonts/*',
            'public/libs/components-font-awesome/fonts/*'
        ])
        .pipe(gulp.dest('public/dist/fonts'));
});

gulp.task('build-css', function () {
    return gulp.src([
            'public/libs/bootstrap/dist/css/bootstrap.css',
            'public/libs/angular-toastr/dist/angular-toastr.css',
            'public/libs/components-font-awesome/css/font-awesome.css',
            'public/css/**/*.css'
        ])
        .pipe(concat('styles.css'))
        .pipe(gutil.env.type === 'production' ? nano() : gutil.noop())
        .pipe(gulp.dest('public/dist/css'));
});

gulp.task('build-js', function () {
    return gulp.src([
            'public/libs/jquery/dist/jquery.js',
            'public/libs/bootstrap/dist/js/bootstrap.js',
            'public/libs/moment/moment.js',
            'public/libs/lodash/lodash.js'
        ])
        .pipe(concat('core.js'))
        .pipe(gutil.env.type === 'production' ? uglify() : gutil.noop())
        .pipe(gulp.dest('public/dist/js'));
});

gulp.task('build-angular', function () {
    return gulp.src([
            'public/libs/angular/angular.js',
            'public/libs/angular-i18n/angular-locale_de-at.js',
            'public/libs/angular-route/angular-route.js',
            'public/libs/angular-moment/angular-moment.js',
            'public/libs/angular-bootstrap/ui-bootstrap-tpls.js',
            'public/libs/angular-local-storage/dist/angular-local-storage.js',
            'public/libs/angular-toastr/dist/angular-toastr.tpls.js'])
        .pipe(concat('angular.js'))
        .pipe(gutil.env.type === 'production' ? uglify() : gutil.noop())
        .pipe(gulp.dest('public/dist/js'));
});

gulp.task('jshint', function () {
    return gulp.src([directories.app])
        .pipe(jshint())
        .pipe(jshint.reporter('default'))
        .pipe(jshint.reporter('fail'));
});

gulp.task('build-app', function () {
    return gulp.src([directories.app])
        .pipe(concat('app.js'))
        .pipe(gutil.env.type === 'production' ? uglify() : gutil.noop())
        .pipe(gulp.dest('public/dist/js'));
});

//watcher
gulp.task('watch', function () {
    gulp.watch('public/js/**/*.js', ['jshint', 'build-app']);
    gulp.watch('public/less/**/*.less', ['build-less', 'build-css']);
});

