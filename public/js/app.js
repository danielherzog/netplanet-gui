angular.module('netplanetControllers', ['angularMoment']);
angular.module('netplanetServices', []);
angular.module('netplanetDirectives', []);
angular.module('netplanetApp', [
    'ngRoute',
    'ui.bootstrap',
    'LocalStorageModule',
    'toastr',
    'netplanetControllers',
    'netplanetServices',
    'netplanetDirectives']
);