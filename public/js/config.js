(function () {
    "use strict";

    angular.module('netplanetApp').value('serviceUrl', 'http://localhost:52380/');

    var run = function ($rootScope, $location, toastr, AuthService) {
        $rootScope.$on('$routeChangeStart', function (event, next) {
            if (next.access !== undefined) {
                var isAuth = AuthService.authentication().isAuth;
                if (next.access.auth && !isAuth) {
                    $location.path('/login');
                }
            }
        });
    };

    var config = function ($locationProvider, $routeProvider, $httpProvider, toastrConfig) {

        $httpProvider.interceptors.push('AuthInterceptorService');

        angular.extend(toastrConfig, {
            progressBar: true
        });

        $routeProvider.when('/', {
            name: 'Dashboard',
            templateUrl: 'views/dashboard.html',
            controller: 'DashboardController',
            controllerAs: 'dashboard',
            access: {
                auth: true
            }
        }).when('/login', {
            name: 'Login',
            templateUrl: 'views/login.html',
            controller: 'LoginController',
            controllerAs: 'login',
            access: {
                auth: false
            }
        }).when('/services', {
            name: 'Serviceverträge',
            nav: true,
            templateUrl: 'views/agreements/list.html',
            controller: 'AgreementListController',
            controllerAs: 'agreementList',
            access: {
                auth: true
            }
        }).when('/services/create', {
            templateUrl: 'views/agreements/create.html',
            controller: 'AgreementCreateController',
            controllerAs: 'agreementCreate',
            access: {
                auth: true
            }
        }).when('/services/detail/:id', {
            templateUrl: 'views/agreements/detail.html',
            controller: 'AgreementDetailController',
            controllerAs: 'agreementDetail',
            access: {
                auth: true
            },
            resolve: {
                service: ['$route', 'AgreementService', function ($route, AgreementService) {
                    return AgreementService.get($route.current.params.id);
                }],
                articles: ['$route', 'AgreementService', function ($route, AgreementService) {
                    return AgreementService.articles($route.current.params.id);
                }]
            }
        }).when('/services/edit/:id', {
            templateUrl: 'views/agreements/edit.html',
            controller: 'AgreementEditController',
            controllerAs: 'agreementEdit',
            access: {
                auth: true
            },
            resolve: {
                service: ['$route', 'AgreementService', function ($route, AgreementService) {
                    return AgreementService.get($route.current.params.id);
                }],
                articles: ['$route', 'AgreementService', function ($route, AgreementService) {
                    return AgreementService.articles($route.current.params.id);
                }]
            }
        }).when('/users', {
            name: 'Benutzer',
            nav: true,
            templateUrl: 'views/users/list.html',
            controller: 'UserListController',
            controllerAs: 'userList',
            access: {
                auth: true
            },
            resolve: {
                users: ['UserService', function (UserService) {
                    return UserService.all();
                }]
            }
        }).when('/users/create', {
            templateUrl: 'views/users/create.html',
            controller: 'UserCreateController',
            controllerAs: 'userCreate',
            access: {
                auth: true
            }
        }).when('/users/detail/:id', {
            templateUrl: 'views/users/detail.html',
            controller: 'UserDetailController',
            controllerAs: 'userDetail',
            access: {
                auth: true
            },
            resolve: {
                user: ['$route', 'UserService', function ($route, UserService) {
                    return UserService.get($route.current.params.id);
                }]
            }
        }).when('/users/edit/:id', {
            templateUrl: 'views/users/edit.html',
            controller: 'UserEditController',
            controllerAs: 'userEdit',
            access: {
                auth: true
            },
            resolve: {
                user: ['$route', 'UserService', function ($route, UserService) {
                    return UserService.get($route.current.params.id);
                }]
            }
        }).otherwise({
            redirectTo: '/login'
        });

        $locationProvider.html5Mode(false).hashPrefix('!');
    };

    run.$inject = ['$rootScope', '$location', 'toastr', 'AuthService'];
    angular.module('netplanetApp').run(run);

    config.$inject = ['$locationProvider', '$routeProvider', '$httpProvider', 'toastrConfig'];
    angular.module('netplanetApp').config(config);
})();

