(function () {
    "use strict";

    var agreementCreateController = function ($scope, $location, toastr, moment, DateService, AgreementService, CustomerService) {

        $scope.service = {};
        $scope.saving = false;

        CustomerService.all().then(function (data) {
            $scope.customers = data;
        });

        DateService.getIntervals().then(function (data) {
            $scope.intervals = data;
        });

        $scope.datepicker = {
            setupdate: false,
            contractdate: false,
            invoicedate: false
        };

        $scope.calcContractDate = function () {
            if ($scope.service.setupdate !== null && $scope.service.interval !== null) {
                $scope.service.contractdate = $scope.service.invoicedate = DateService.getContractDate($scope.service.interval.id, $scope.service.setupdate);
            }
        };

        $scope.save = function () {
            $scope.saving = true;
            var model = angular.copy($scope.service);
            model.customer = $scope.service.customer.id;
            model.interval = $scope.service.interval.id;

            AgreementService.add(model).then(function (response) {
                $scope.saving = false;
                toastr.success('Der Servicevertrag wurde erfolgreich erstellt', 'Erstellt');
                $location.path('/services');
            }, function () {
                $scope.saving = false;
                toastr.error('Der Servicevertrag konnte nicht erstellt werden', 'Fehler');
            });
        };

        $scope.open = function (picker) {
            $scope.datepicker[picker] = true;
        };
    };

    agreementCreateController.$inject = ['$scope', '$location', 'toastr', 'moment', 'DateService', 'AgreementService', 'CustomerService'];
    angular.module('netplanetControllers').controller('AgreementCreateController', agreementCreateController);
})();