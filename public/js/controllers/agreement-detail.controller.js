(function () {
    "use strict";

    var agreementDetailController = function ($scope, $uibModal, $location, service, articles) {
        $scope.service = service;
        $scope.articles = articles;

        $scope.open = function () {
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: 'views/modal/delete-service.html',
                controller: 'ModalDeleteServiceController',
                resolve: {
                    id: function () {
                        return $scope.service.id;
                    }
                }
            });

            modalInstance.result.then(function () {
                $location.path('/services');
            }, function () {

            });
        };
    };

    agreementDetailController.$inject = ['$scope', '$uibModal', '$location', 'service', 'articles'];
    angular.module('netplanetControllers').controller('AgreementDetailController', agreementDetailController);
})();