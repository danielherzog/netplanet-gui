(function () {
    "use strict";

    var agreementEditController = function ($scope, $routeParams, $uibModal, toastr, moment, DateService, AgreementService, CustomerService, service, articles) {
        $scope.saving = false;
        $scope.service = service;
        $scope.articles = articles;
        $scope.service.setupdate = new Date(service.setupdate);
        $scope.service.contractdate = new Date(service.contractdate);
        $scope.service.invoicedate = new Date(service.invoicedate);
        $scope.service.nextbillingdate = new Date(service.nextbillingdate);
        $scope.service.nextnoticedate = new Date(service.nextnoticedate);
        $scope.service.interval = DateService.getInterval(service.interval);

        CustomerService.get(service.customer).then(function (data) {
            $scope.service.customer = data;
        });

        CustomerService.all().then(function (data) {
            $scope.customers = data;
        });

        DateService.getIntervals().then(function (data) {
            $scope.intervals = data;
        });

        AgreementService.linked().then(function (data) {
            $scope.linked = data;
        });

        $scope.datepicker = {
            setupdate: false,
            contractdate: false,
            invoicedate: false
        };

        $scope.save = function () {
            $scope.saving = true;
            var model = angular.copy($scope.service);
            model.customer = $scope.service.customer.id;
            model.interval = $scope.service.interval.id;

            AgreementService.update($scope.service.id, model).then(function (response) {
                $scope.saving = false;
                toastr.success('Der Servicevertrag wurde erfolgreich gespeichert', 'Aktualisiert');
            }, function () {
                $scope.saving = false;
                toastr.error('Der Servicevertrag konnte nicht gespeichert werden', 'Fehler');
            });
        };

        $scope.open = function (picker) {
            $scope.datepicker[picker] = true;
        };

        $scope.addLink = function (id, linkid) {
            AgreementService.addLink(id, linkid).then(function (response) {
                toastr.success('Die Verknüpfung wurde erfolgreich erstellt', 'Verknüpfung erstellt');
            });
        };

        $scope.deleteLink = function (id) {
            AgreementService.deleteLink(id).then(function (response) {
                toastr.success('Die Verknüpfung wurde erfolgreich gelöscht', 'Verknüpfung gelöscht');
            });
        };

        $scope.openAddArticle = function () {
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: 'views/modal/add-article.html',
                controller: 'ModalAddArticleController',
                resolve: {
                    id: function () {
                        return $scope.service.id;
                    }
                }

            });

            modalInstance.result.then(function () {
                AgreementService.articles($scope.service.id).then(function (data) {
                    $scope.articles = data;
                });
            }, function () {

            });
        };
    };

    agreementEditController.$inject = ['$scope', '$routeParams', '$uibModal', 'toastr', 'moment', 'DateService', 'AgreementService', 'CustomerService', 'service', 'articles'];
    angular.module('netplanetControllers').controller('AgreementEditController', agreementEditController);
})();