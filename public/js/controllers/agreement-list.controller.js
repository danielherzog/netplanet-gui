(function () {
    "use strict";

    var agreementListController = function ($scope, AgreementService) {
        $scope.displayInactive = false;
        $scope.searchterm = '';

        AgreementService.all('', false).then(function (data) {
            $scope.items = data;
        });

        $scope.search = function () {
            AgreementService.all($scope.searchterm, $scope.displayInactive).then(function (data) {
                $scope.items = data;
            });
        };

        $scope.reset = function () {
            AgreementService.all('', $scope.displayInactive).then(function (data) {
                $scope.items = data;
            });
        };
    };

    agreementListController.$inject = ['$scope', 'AgreementService'];
    angular.module('netplanetControllers').controller('AgreementListController', agreementListController);
})();