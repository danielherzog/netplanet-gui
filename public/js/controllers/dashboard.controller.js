(function () {
    "use strict";

    var dashboardController = function ($location, toastr, AgreementService) {

        var self = this;

        self.stats = {
            activeServices: 0,
            sum: 0
        };

        AgreementService.count().then(function (data) {
            self.stats.activeServices = data;
        });

        AgreementService.sum().then(function(data){
            self.stats.sum = data;
        });
    };

    dashboardController.$inject = ['$location', 'toastr', 'AgreementService'];

    angular.module('netplanetControllers').controller('DashboardController', dashboardController);
})();