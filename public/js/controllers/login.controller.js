(function () {
    "use strict";

    var loginController = function ($rootScope, $location, toastr, AuthService) {
        var self = this;

        self.login = function () {
            AuthService.login(self.username, self.password).then(function () {
                toastr.success(self.username + ' erfolgreich eingeloggt', 'Login');
                $rootScope.$broadcast('login');
                $location.path('/');
            }, function(){
                toastr.error('Die angegebenen Logindaten sind nicht korrekt','Login fehlerhaft');
            });
        };
    };

    loginController.$inject = ['$rootScope', '$location', 'toastr', 'AuthService'];
    angular.module('netplanetControllers').controller('LoginController', loginController);
})();