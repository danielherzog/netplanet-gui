(function () {
    "use strict";

    var modalAddArticleController = function ($scope, $uibModalInstance, toastr, id, AgreementService, ArticleService) {

        $scope.saving = false;
        $scope.article = {};

        ArticleService.all().then(function (data) {
            $scope.articles = data;
        });

        $scope.types = [
            {value: 'singular', name: 'Einmalig'},
            {value: 'recurring', name: 'Wiederkehrend'},
            {value: 'volume', name: 'Volumenbasierend'}
        ];

        $scope.ok = function () {
            $scope.saving = true;
            var article = angular.copy($scope.article);
            article.article = $scope.article.article.id;
            article.type = $scope.article.type.value;

            AgreementService.addArticle(id, article).then(function () {
                $scope.saving = false;
                toastr.success('Der Artikel wurde erfolgreich gespeichert', 'Artikel hinzugefügt');
                $uibModalInstance.close();
            }, function () {
                $scope.saving = false;
                toastr.error('Der Artikel konnte nicht gespeichert werden', 'Fehler beim Hinzufügen');
            });
        };

        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };
    };

    modalAddArticleController.$inject = ['$scope', '$uibModalInstance', 'toastr', 'id', 'AgreementService', 'ArticleService'];
    angular.module('netplanetControllers').controller('ModalAddArticleController', modalAddArticleController);
})();