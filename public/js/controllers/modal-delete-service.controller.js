(function () {
    "use strict";

    var modalDeleteServiceController = function ($scope, $uibModalInstance, toastr, id, AgreementService) {

        $scope.deleting = false;

        $scope.ok = function () {
            $scope.deleting = true;
            AgreementService.delete(id).then(function () {
                $scope.deleting = false;
                toastr.info('Der Servicevertrag wurde erfolgreich gelöscht', 'Servicevertrag gelöscht');
                $uibModalInstance.close();
            }, function () {
                $scope.deleting = false;
                toastr.error('Der Servicevertrag konnte nicht gelöscht werden', 'Fehler beim Löschen');
            });
        };

        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };
    };

    modalDeleteServiceController.$inject = ['$scope', '$uibModalInstance', 'toastr', 'id', 'AgreementService'];
    angular.module('netplanetControllers').controller('ModalDeleteServiceController', modalDeleteServiceController);
})();