(function () {
    "use strict";

    var modalDetailArticleController = function ($scope, $uibModalInstance, article) {

        $scope.article = article;

        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };
    };

    modalDetailArticleController.$inject = ['$scope', '$uibModalInstance', 'article'];
    angular.module('netplanetControllers').controller('ModalDetailArticleController', modalDetailArticleController);
})();