(function () {
    "use strict";

    var modalEditArticleController = function ($scope, $uibModalInstance, toastr, id, articleId, article, AgreementService, ArticleService) {

        $scope.saving = false;
        $scope.article = article;

        ArticleService.all().then(function (data) {
            $scope.articles = data;
        });

        ArticleService.get(article.article).then(function (data) {
            $scope.article.article = data;
        });

        ArticleService.types().then(function (data) {
            $scope.types = data;
        });

        ArticleService.type(article.type).then(function (data) {
            $scope.article.type = data;
        });

        $scope.ok = function () {
            $scope.saving = true;
            var tmp = angular.copy($scope.article);
            tmp.article = $scope.article.article.id;
            tmp.type = $scope.article.type.value;

            AgreementService.updateArticle(id, articleId, tmp).then(function () {
                $scope.saving = false;
                toastr.success('Der Artikel wurde erfolgreich gespeichert', 'Artikel gespeichert');
                $uibModalInstance.close();
            }, function () {
                $scope.saving = false;
                toastr.error('Der Artikel konnte nicht gespeichert werden', 'Fehler beim Aktualisieren');
            });
        };

        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };
    };

    modalEditArticleController.$inject = ['$scope', '$uibModalInstance', 'toastr', 'id', 'articleId', 'article', 'AgreementService', 'ArticleService'];
    angular.module('netplanetControllers').controller('ModalEditArticleController', modalEditArticleController);
})();