(function () {
    "use strict";

    var userCreateController = function ($location, toastr, UserService) {

        var self = this;
        self.user = {};
        self.saving = false;

        self.save = function () {
            self.saving = true;
            var user = angular.copy(self.user);
            UserService.add(user).then(function () {
                self.saving = false;
                toastr.success('Der Benutzer ' + user.username + ' wurde erfolgreich erstellt','Benutzer erstellt');
                $location.path('/users');
            }, function () {
                self.saving = false;
                toastr.error('Der Benutzer ' + user.username + ' konnte nicht erstellt werden','Fehler');
            });
        };
    };

    userCreateController.$inject = ['$location', 'toastr', 'UserService'];
    angular.module('netplanetControllers').controller('UserCreateController', userCreateController);
})();