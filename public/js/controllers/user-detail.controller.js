(function () {
    "use strict";

    var userDetailController = function (user) {
        this.user = user;
    };

    userDetailController.$inject = ['user'];
    angular.module('netplanetControllers').controller('UserDetailController', userDetailController);
})();