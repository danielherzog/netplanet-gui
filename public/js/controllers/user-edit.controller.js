(function () {
    "use strict";

    var userEditController = function ($location, toastr, UserService, user) {

        var self = this;
        self.user = user;
        self.saving = false;

        self.save = function (form) {
            if (form.$valid) {
                self.saving = true;
                var user = angular.copy(self.user);
                UserService.update(user.id, user).then(function () {
                    self.saving = false;
                    toastr.success('Der Benutzer ' + user.username + ' wurde erfolgreich gespeichert', 'Benutzer aktualisiert');
                }, function () {
                    self.saving = false;
                    toastr.error('Der Benutzer ' + user.username + ' konnte nicht gespeichert werden', 'Fehler');
                });
            }
        };
    };

    userEditController.$inject = ['$location', 'toastr', 'UserService', 'user'];
    angular.module('netplanetControllers').controller('UserEditController', userEditController);
})();