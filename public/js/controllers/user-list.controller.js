(function () {
    "use strict";

    var userListController = function (users) {
        this.items = users;
    };

    userListController.$inject = ['users'];
    angular.module('netplanetControllers').controller('UserListController', userListController);
})();