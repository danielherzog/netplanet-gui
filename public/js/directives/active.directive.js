(function () {
    "use strict";
    angular.module('netplanetDirectives')
        .directive('serviceActive', [function () {
            return {
                restrict: 'A',
                scope: {
                    serviceActive: '='
                },
                template: '<span class="label label-success" data-ng-if="active">Aktiv</span>' +
                '<span class="label label-danger" data-ng-if="!active">Inaktiv</span>',
                link: function (scope, element, attrs) {
                    scope.active = scope.serviceActive;
                }
            };
        }]);
})();