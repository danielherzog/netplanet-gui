(function () {
    "use strict";
    angular.module('netplanetDirectives')
        .directive('article', ['$timeout', 'ArticleService', function ($timeout, ArticleService) {
            return {
                restrict: 'A',
                scope: {
                    article: '@'
                },
                replace: true,
                template: '<span>{{nr}} - {{name}}</span>',
                link: function (scope, element, attrs) {
                    ArticleService.get(attrs.article).then(function (data) {
                        scope.name = data.name;
                        scope.nr = data.nr;
                    });
                }
            };
        }]);
})();