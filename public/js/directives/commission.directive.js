(function () {
    "use strict";

    var commissionDirective = function () {
        return {
            restrict: 'A',
            scope: {
                commission: '='
            },
            replace: true,
            template: '<span>{{value}} - {{agent}}</span>',
            link: function (scope, element, attrs) {
                switch(scope.commission.type){
                    case 'percent':
                        scope.value = scope.commission.value + '%';
                        break;

                    case 'absolute':
                        scope.value = scope.commission.value;
                        break;
                }

                scope.agent = scope.commission.agent;
            }
        };
    };

    angular.module('netplanetDirectives').directive('commission', commissionDirective);
})();