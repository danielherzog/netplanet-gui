(function () {
    "use strict";
    angular.module('netplanetDirectives')
        .directive('customer', ['$timeout', 'CustomerService', function ($timeout, CustomerService) {
            return {
                restrict: 'E',
                scope: {
                    id: '='
                },
                replace: true,
                template: '<span>{{id}} - {{name}}</span>',
                link: function (scope, element, attrs) {
                    CustomerService.get(scope.id).then(function (data) {
                        scope.name = data.name;
                        scope.id = data.id;
                    });
                }
            };
        }]);
})();