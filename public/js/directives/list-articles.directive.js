(function () {
    "use strict";

    var listArticlesDirective = function ($uibModal, AgreementService, toastr) {
        return {
            restrict: 'E',
            scope: {
                items: '=',
                serviceId: '=',
                edit: '@',
                detail: '@'
            },
            replace: true,
            templateUrl: 'views/directives/list-articles.html',
            link: function (scope, elem, attrs) {
                scope.edit = false;
                scope.detail = false;
                scope.delete = false;

                if (attrs.edit) {
                    scope.edit = true;
                }

                if (attrs.detail) {
                    scope.detail = true;
                }

                if (attrs.delete) {
                    scope.delete = true;
                }

                scope.openDetailArticle = function(articleId){
                    var modalInstance = $uibModal.open({
                        animation: true,
                        templateUrl: 'views/modal/detail-article.html',
                        controller: 'ModalDetailArticleController',
                        resolve: {
                            article: function () {
                                return AgreementService.article(scope.serviceId, articleId);
                            }
                        }
                    });
                };

                scope.openEditArticle = function (articleId) {
                    var modalInstance = $uibModal.open({
                        animation: true,
                        templateUrl: 'views/modal/edit-article.html',
                        controller: 'ModalEditArticleController',
                        resolve: {
                            id: function () {
                                return scope.serviceId;
                            },
                            articleId: function () {
                                return articleId;
                            },
                            article: function () {
                                return AgreementService.article(scope.serviceId, articleId);
                            }
                        }

                    });

                    modalInstance.result.then(function () {
                        AgreementService.articles(scope.serviceId).then(function (data) {
                            scope.items = data;
                        });
                    }, function () {

                    });
                };

                scope.deleteArticle = function (index, articleId) {
                    AgreementService.deleteArticle(scope.serviceId, articleId).then(function () {
                        scope.items.splice(index, 1);
                        toastr.success('Der Artikel wurde erfolgreich entfernt', 'Artikel entfernt');
                    });
                };
            }
        };
    };

    listArticlesDirective.$inject = ['$uibModal', 'AgreementService', 'toastr'];
    angular.module('netplanetDirectives').directive('listArticles', listArticlesDirective);
})();