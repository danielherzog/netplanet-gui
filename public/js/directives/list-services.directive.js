(function () {
    "use strict";
    angular.module('netplanetDirectives')
        .directive('listServices', [function () {
            return {
                restrict: 'E',
                scope: {
                    items: '=',
                    edit: '@',
                    detail: '@'
                },
                replace: true,
                templateUrl: 'views/directives/list-services.html',
                link: function (scope, elem, attrs) {
                    scope.edit = false;
                    scope.detail = false;

                    if (attrs.edit) {
                        scope.edit = true;
                    }

                    if (attrs.detail) {
                        scope.detail = true;
                    }
                }
            };
        }]);
})();