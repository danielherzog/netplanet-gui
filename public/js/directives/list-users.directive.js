(function () {
    "use strict";

    var listUserDirective = function (UserService, toastr) {
        return {
            restrict: 'E',
            scope: {
                items: '=',
                edit: '@',
                detail: '@'
            },
            replace: true,
            templateUrl: 'views/directives/list-users.html',
            link: function (scope, elem, attrs) {
                scope.edit = false;
                scope.detail = false;
                scope.delete = false;

                if (attrs.edit) {
                    scope.edit = true;
                }

                if (attrs.detail) {
                    scope.detail = true;
                }

                if (attrs.delete) {
                    scope.delete = true;
                }

                scope.deleteUser = function (index, userId) {
                    UserService.delete(userId).then(function () {
                        scope.items.splice(index, 1);
                        toastr.success('Der Benutzer wurde erfolgreich entfernt', 'Benutzer entfernt');
                    });
                };
            }
        };
    };

    listUserDirective.$inject = ['UserService', 'toastr'];
    angular.module('netplanetDirectives').directive('listUsers', listUserDirective);
})();