(function () {
    "use strict";
    angular.module('netplanetDirectives')
        .directive('navbar', [function () {
            return {
                restrict: 'E',
                replace: true,
                transclude: false,
                scope: {
                    title: '@'
                },
                controller: ['$scope', '$location', '$route', 'toastr', 'AuthService', function ($scope, $location, $route, toastr, AuthService) {
                    $scope.prefix = '#!';
                    $scope.username = AuthService.authentication().username;

                    function fill() {
                        $scope.routes = [];
                        angular.forEach($route.routes, function (value, key) {
                            if (value.nav && value.access.auth == AuthService.authentication().isAuth) {
                                $scope.routes.push({
                                    path: value.originalPath,
                                    name: value.name
                                });
                            }
                        });
                    }

                    fill();

                    $scope.$on('login', function () {
                        fill();
                        $scope.username = AuthService.authentication().username;
                    });

                    $scope.$on('logout', function () {
                        fill();
                        $scope.username = null;
                    });

                    $scope.logout = function () {
                        AuthService.logout();
                        $scope.$broadcast('logout');
                        toastr.info('Sie wurden erfolgreich ausgeloggt', 'Logout');
                        $location.path('/login');
                    };
                }],
                templateUrl: 'views/directives/nav.html'
            };
        }]);
})();