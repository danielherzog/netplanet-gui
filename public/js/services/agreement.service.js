(function () {
    "use strict";

    var agreementService = function ($http, serviceUrl) {
        var serviceBase = serviceUrl + 'api/Agreement/';
        return {
            all: function (term, inactive) {
                return $http.get(serviceBase + '?term=' + term + '&inactive=' + inactive).then(function (response) {
                    return response.data;
                });
            },
            linked: function () {
                return $http.get(serviceBase + 'Linked').then(function (response) {
                    return response.data;
                });
            },
            get: function (id) {
                return $http.get(serviceBase + id).then(function (response) {
                    return response.data;
                });
            },
            count: function () {
                return $http.get(serviceBase + 'Count').then(function (response) {
                    return response.data;
                });
            },
            sum: function () {
                return $http.get(serviceBase + 'Sum').then(function (response) {
                    return response.data;
                });
            },
            add: function (service) {
                return $http.post(serviceBase, service);
            },
            update: function (id, service) {
                return $http.put(serviceBase + id, service);
            },
            delete: function (id) {
                return $http.delete(serviceBase + id);
            },
            addLink: function (id, linkid) {
                return $http.put(serviceBase + id + '/Link/' + linkid);
            },
            deleteLink: function (id) {
                return $http.delete(serviceBase + id + '/Link');
            },
            articles: function (id) {
                return $http.get(serviceBase + id + '/Articles').then(function (response) {
                    return response.data;
                });
            },
            article: function(id, articleId){
                return $http.get(serviceBase + id + '/Articles/' + articleId).then(function (response) {
                    return response.data;
                });
            },
            addArticle: function (id, article) {
                return $http.post(serviceBase + id + '/Articles', article);
            },
            updateArticle: function (id, articleId, article) {
                return $http.put(serviceBase + id + '/Articles/' + articleId, article);
            },
            deleteArticle: function (id, articleId) {
                return $http.delete(serviceBase + id + '/Articles/' + articleId);
            }
        };
    };

    agreementService.$inject = ['$http', 'serviceUrl'];
    angular.module('netplanetServices').factory('AgreementService', agreementService);
})();