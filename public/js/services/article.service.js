(function () {
    "use strict";

    var articleService = function ($http, $q, _, serviceUrl) {

        var base = serviceUrl + 'api/Article/';

        var articles = null;

        var types = [
            {value: 'singular', name: 'Einmalig'},
            {value: 'recurring', name: 'Wiederkehrend'},
            {value: 'volume', name: 'Volumenbasierend'}
        ];

        var getArticle = function (deferred, id) {
            var a = _.find(articles, 'id', id);

            if (a) {
                deferred.resolve(a);
            }
            else {
                deferred.reject('not found');
            }
        };

        return {
            all: function () {
                var deferred = $q.defer();

                if (!articles) {
                    $http.get(base + 'All').then(function (response) {
                        articles = response.data;
                        deferred.resolve(articles);
                    });
                }
                else {
                    deferred.resolve(articles);
                }

                return deferred.promise;
            },
            get: function (id) {
                id = parseInt(id);
                var deferred = $q.defer();

                if (!articles) {
                    $http.get(base + 'All').then(function (response) {
                        articles = response.data;
                        getArticle(deferred, id);
                    });
                }
                else {
                    getArticle(deferred, id);
                }
                return deferred.promise;
            },
            types: function () {
                return $q.when(types);
            },
            type: function (id) {
                return $q.when(_.find(types, 'value', id));
            }
        };
    };

    articleService.$inject = ['$http', '$q', '_', 'serviceUrl'];
    angular.module('netplanetServices').factory('ArticleService', articleService);
})();