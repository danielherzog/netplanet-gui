(function () {
    "use strict";

    var authService = function ($http, $q, localStorageService, serviceUrl) {
        var serviceBase = serviceUrl;
        var key = 'authorizationData';

        return {
            authentication: function () {
                var _auth = {
                    isAuth: false,
                    username: ''
                };

                var authData = localStorageService.get(key);
                if (authData) {
                    _auth.isAuth = true;
                    _auth.username = authData.userName;
                }

                return _auth;
            },
            logout: function () {
                localStorageService.remove(key);
            },
            login: function (username, password) {
                var deferred = $q.defer();
                var data = "grant_type=password&username=" + username + "&password=" + password;

                $http.post(serviceBase + 'token', data, {headers: {'Content-Type': 'application/x-www-form-urlencoded'}}).success(function (response) {
                    localStorageService.set(key, {
                        token: response.access_token,
                        userName: username
                    });
                    deferred.resolve(response);
                }).error(function (err, status) {
                    localStorageService.remove(key);
                    deferred.reject(err);
                });

                return deferred.promise;
            }
        };
    };

    authService.$inject = ['$http', '$q', 'localStorageService', 'serviceUrl'];
    angular.module('netplanetServices').factory('AuthService', authService);
})();