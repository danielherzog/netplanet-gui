(function () {
    "use strict";

    var customerService = function ($http, $q, _, serviceUrl) {
        var base = serviceUrl + 'api/Customer/';
        var customers = null;

        return {
            all: function () {
                var deferred = $q.defer();

                if (!customers) {
                    $http.get(base + 'All').then(function (response) {
                        customers = response.data;
                        deferred.resolve(customers);
                    });
                }
                else {
                    deferred.resolve(customers);
                }

                return deferred.promise;
            },
            get: function (id) {
                id = parseInt(id);
                var deferred = $q.defer();

                if (!customers) {
                    $http.get(base + 'All').then(function (response) {
                        customers = response.data;
                        deferred.resolve(_.find(customers, 'id', id));
                    });
                }
                else {
                    deferred.resolve(_.find(customers, 'id', id));
                }
                return deferred.promise;
            }
        };
    };

    customerService.$inject = ['$http', '$q', '_', 'serviceUrl'];
    angular.module('netplanetServices').factory('CustomerService', customerService);
})();