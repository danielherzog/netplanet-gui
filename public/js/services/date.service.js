(function () {
    "use strict";

    var dateService = function (moment, _, $q) {
        var intervals = [
            {id: 1, name: 'monatlich'},
            {id: 2, name: 'quartal'},
            {id: 3, name: 'halbjährlich'},
            {id: 4, name: 'jährlich'}
        ];

        return {
            getIntervals: function () {
                return $q.when(intervals);
            },
            getInterval: function (id) {
                return _.find(intervals, function (interval) {
                    return interval.id == id;
                });
            },
            getContractDate: function (interval, setupdate) {
                var contractDate = moment(setupdate);
                switch (interval) {
                    case 1:
                        contractDate.add(1, 'months').startOf('month');
                        return contractDate.toDate();

                    case 2:
                        if (contractDate.month() < 3)
                            return moment([contractDate.year(), 3, 1]).toDate();
                        else if (contractDate.month() > 3 && contractDate.month() < 6)
                            return moment([contractDate.year(), 6, 1]).toDate();
                        else if (contractDate.month() > 6 && contractDate.month() < 9)
                            return moment([contractDate.year(), 9, 1]).toDate();
                        else
                            return moment([contractDate.year() + 1, 0, 1]).toDate();
                        break;
                    case 3:
                        if (contractDate.month() < 6)
                            return moment([contractDate.year(), 6, 1]).toDate();
                        else
                            return moment([contractDate.year() + 1, 0, 1]).toDate();
                        break;

                    case 4:
                        return moment([contractDate.year() + 1, 0, 1]).toDate();
                }
            }
        };
    };

    dateService.$inject = ['moment', '_', '$q'];
    angular.module('netplanetServices').factory('DateService', dateService);
})();