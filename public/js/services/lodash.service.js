(function () {
    "use strict";

    var _ = function ($window) {
        return $window._;
    };

    _.$inject = ['$window'];
    angular.module('netplanetServices').factory('_', _);
})();