(function () {
    "use strict";

    var userService = function ($http, serviceUrl) {
        var serviceBase = serviceUrl + 'api/Users/';
        return {
            all: function () {
                return $http.get(serviceBase).then(function (response) {
                    return response.data;
                });
            },
            get: function (id) {
                return $http.get(serviceBase + id).then(function (response) {
                    return response.data;
                });
            },
            add: function (user) {
                return $http.post(serviceBase, user);
            },
            update: function (id, user) {
                return $http.put(serviceBase + id, user);
            },
            delete: function (id) {
                return $http.delete(serviceBase + id);
            }
        };
    };

    userService.$inject = ['$http', 'serviceUrl'];
    angular.module('netplanetServices').factory('UserService', userService);
})();